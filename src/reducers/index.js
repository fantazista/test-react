import { combineReducers } from 'redux';
import categories from './categories.js';
import products from './products';
import visibilityFilter from './visibilityFilter';

export default combineReducers({
    products,
    categories,
    visibilityFilter
});
