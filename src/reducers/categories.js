import initState from '../api/categories.json';

function categories(state = initState, action) {
    switch (action.type) {
        case 'ADD_CATEGORY': {
            return [...state, action.payload];
        }

        case 'EDIT_CATEGORY': {
            return state.map(item => {
                if (item.id === action.payload.id) {
                    return {...item, ...action.payload}
                }

                return item
            });
        }

        default: {
            return state;
        }
    }
}

export default categories;
