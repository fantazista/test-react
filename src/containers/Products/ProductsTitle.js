import React from 'react';
import styles from './Products.module.css';
import {connect} from "react-redux";


class ProductsTitle extends React.Component {
    showProduct = (index) => {
        this.props.dispatchShowProduct(index);
    }

    editProductTitle = (event, id) => {
        let productOptions = {
            id: id,
            title: event.target.value
        }

        this.props.dispatchEditProductTitle(productOptions)
    }

    render() {
        console.log(333);
        return (
            <div>
                <span className={`${styles.Product__Name} ${ this.props.editMode && styles.hide}`}
                    onClick={()=> this.showProduct(this.props.id)}>
                    {this.props.name}
                </span>

                <input className={`${styles.Product__Input} ${ this.props.editMode && styles.block}`}
                       type="text"
                       value={this.props.name}
                       onChange={(event) => this.editProductTitle(event, +this.props.id)}
                />
            </div>
        )
    }
}

export default connect(null,
    dispatch => ({
        dispatchShowProduct: (index) => {
            dispatch({type: 'SHOW_PRODUCT', payload: index})
        },

        dispatchEditProductTitle: (productOpt) => {
            dispatch({type: 'EDIT_PRODUCT_TITLE', payload: productOpt})
        }
    })
)(ProductsTitle);
