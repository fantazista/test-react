import React from 'react';
import {connect} from 'react-redux';
import ProductsItem from '../Products/ProductsItem.js';
import AddProduct from '../Products/AddProduct.js';
import plus from "../../assets/plus.svg";
import styles from './Products.module.css';

class Products extends React.Component {
    showAddProduct = (boolean) => {
        this.props.dispatchShowAddProduct(boolean);
    };

    render() {

        const ProductsList = this.props.products.map(function(item) {
            return <ProductsItem key={item.id} id={item.id} name={item.title} editMode={item.editMode} />
        });

        return (
            <div className={styles._Coloumn}>

                <button className={styles.ThumbButton} onClick={() => this.showAddProduct(true)} > Məhsullar
                    <img className={styles.ThumbButton__img} src={plus} alt="#"/>
                </button>

                <AddProduct showAddProduct={this.props.showAddProduct}/>

                <ul className='ProductList'>
                    { ProductsList }
                </ul>
            </div>
        )
    }
}

export default connect(
    state => ({
        products: state.products,
        showAddProduct: state.visibilityFilter.showAddProduct,
    }),

    dispatch => ({
        dispatchShowAddProduct: (boolean) => {
            dispatch({type: 'SHOW_ADD_PRODUCT', payload: boolean});
        },
    })
)(Products);
