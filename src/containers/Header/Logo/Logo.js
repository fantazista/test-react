import React from 'react';
import styles from './Logo.module.css';
import logo from './logo.svg';

const Logo = () => {
    return(
        <h1 className='m-0 flex-shrink-0'>
            <a href="#" className={styles.Logo}>
                <img className={styles.Logo__img} src={logo} alt=""/>
            </a>
        </h1>
    )
};

export default Logo;
