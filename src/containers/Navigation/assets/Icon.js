import React from 'react';
import styles from './Icon.module.css';

const Icon = (props) => {
    return(
        <div className={styles.IconItem}>
            <img src={props.url} className={styles.IconItem__img} alt="#" />

            <span className={styles.IconItem__span}>
                {props.text}
            </span>
        </div>
    )
};

export default Icon;