import React from 'react';
import Categories from './Categories/Categories.js';
import Products from './Products/Products.js';
import EditProduct from './EditProduct/EditProduct.js';
import styles from './MainPanel.module.css';

class MainPanel extends React.Component {
    render() {
        return (
            <div className={styles.ContainerMain}>
                <span className={styles.Date}>08 - 18 Sentyabr 2018</span>

                <div className={styles.ThumbsWrapper}>
                    <Categories />

                    <Products />

                    <EditProduct />
                </div>
            </div>
        )
    }
}

export default MainPanel;
