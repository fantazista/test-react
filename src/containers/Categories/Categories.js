import React from 'react';
import styles from './Categories.module.css';
import plus from "../../assets/plus.svg";
import Category from "./Category";

class Categories extends React.Component {
    render() {
        return (
            <div className={styles._Coloumn}>
                <button className={styles._ThumbButton}> Kategoriyalar
                    <img className={styles._ThumbButton__img} src={plus} alt="#"/>
                </button>

                <Category />
            </div>
        )
    }
}

export default Categories;
