import React from 'react';
import {connect} from 'react-redux';
import styles from './Categories.module.css';
import select_arrow from '../Navigation/assets/select_arrow.svg';

class Category extends React.Component {
    render() {
        return this.props.categories.map((item) => {
            return (
                <div className={styles._ThumbItem} key={item.id}>
                    <img className={styles._ThumbItem__img} src={select_arrow} alt="#"/>
                    {item.name}
                </div>
            )
        })
    }
}

export default connect(
    state => ({
        categories: state.categories,
    }),

)(Category);
