import React from 'react';
import './App.css';
import Header from './containers/Header/Header';
import Navigation from './containers/Navigation/Navigation';
import MainPanel from "./containers/MainPanel";

class App extends React.Component {

    render() {
        return (
            <div className="App">
                <Header/>

                <Navigation/>

                <MainPanel />
            </div>
        );
    }
}

export default App;
